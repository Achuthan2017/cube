
/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: ardu_motor_controller.ino
        Version:   v0.0
        Date:      07-11-2019
    ============================================================================
*/

/*
** =============================================================================
**                        INCLUDE STATEMENTS
** =============================================================================
*/


/*
** =============================================================================
**                        DEFINES AND MACROS
** =============================================================================
*/

#include <SPI.h>
#include <NRFLite.h>
#include <Wire.h>


/*
** =============================================================================
**                       Function Prototypes
** =============================================================================
*/




/*
** =============================================================================
**                        Global Variables
** =============================================================================
*/



/*********************************************************/
const static uint8_t RADIO_ID = 0;
const static uint8_t DESTINATION_RADIO_ID = 1;
const static uint8_t PIN_RADIO_CE = 7;
const static uint8_t PIN_RADIO_CSN = 8;
const static uint8_t PIN_RADIO_IRQ = 2;

enum RadioPacketType
{
  Heartbeat,
  BeginGetData,
  EndGetData,
  ReceiverData
};

struct RadioPacket
{
  RadioPacketType PacketType;
  uint8_t FromRadioId;
  uint8_t Direction;
  uint8_t Speed;
  uint8_t Motor1Tmp;
  uint8_t Motor2Tmp;
  uint32_t Uptime;
};


NRFLite _radio;
volatile uint8_t _dataWasReceived;
uint32_t _lastHeartbeatSendTime;
uint32_t _lastCurrentSendTime;


/*********************************************************/


/*
** =============================================================================
**                        FUNCTION DECLARATION
** =============================================================================
*/

/*==============================================================================
** Function...: setup
** Return.....: void
** Description: Initialization function
** Created....: 26.06.2017 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void setup() {

  Serial.begin( 115200 );

  if (!_radio.init(RADIO_ID, PIN_RADIO_CE, PIN_RADIO_CSN))
  {
    Serial.println("Cannot communicate with radio");
    while (1); // Wait here forever.
  }


  attachInterrupt(digitalPinToInterrupt(PIN_RADIO_IRQ), radioInterrupt, FALLING);



  Serial.println( "Prime Motor Driver Ready" );
}


/*==============================================================================
** Function...: loop
** Return.....: void
** Description: Main function 
** Created....: 26.06.2017 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void loop() {

  if (_dataWasReceived)
  {
    _dataWasReceived = false;
    while (_radio.hasDataISR())
    {
      Serial.println("Got data");
      RadioPacket radioData;
      _radio.readData(&radioData);
      handleRadioIn(&radioData);
    }
  }

  // Send a heartbeat once every 2-seconds.
  if (millis() - _lastHeartbeatSendTime > 1998)
  {
    _lastHeartbeatSendTime = millis();
    
    RadioPacket  heartBeatData;
    heartBeatData.PacketType = Heartbeat;
    heartBeatData.FromRadioId = RADIO_ID;
    heartBeatData.Uptime =  _lastHeartbeatSendTime / 1000;;
    sendRadioData(heartBeatData);
  }
}



void sendRadioData( RadioPacket  radioData )
{
  if (_radio.send(DESTINATION_RADIO_ID, &radioData, sizeof(radioData)))
  {
    Serial.println("Success");
  }
  else
  {
    Serial.println("Failed");
  }

  _radio.startRx();
}

void handleRadioIn( RadioPacket  *radioData )
{
  if ( radioData->Direction == 0 )
  {
    Serial.println("...Failed");
  }
  else if ( radioData->Direction == 1 )
  {

    Serial.print("down");
  }
  else if ( radioData->Direction == 2 )
  {

    
    Serial.print("up ");
  }
  else if ( radioData->Direction == 3 )
  {

    Serial.print("left ");
  }
  else if ( radioData->Direction == 4 )
  {


    Serial.print("right ");
  }
  else if ( radioData->Direction == 5 )
  {
    Serial.print("stop ");

  }

  Serial.println(radioData->Speed);


}




void radioInterrupt()
{
  // Ask the radio what caused the interrupt.  This also resets the IRQ pin on the
  // radio so a new interrupt can be triggered.

  uint8_t txOk, txFail, rxReady;
  _radio.whatHappened(txOk, txFail, rxReady);

  // txOk = the radio successfully transmitted data.
  // txFail = the radio failed to transmit data.
  // rxReady = the radio received data.

  if (rxReady)
  {
    _dataWasReceived = true;
  }
}
