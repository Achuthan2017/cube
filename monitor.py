#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" [Module Description]


"""

import threading
import time


class Monitor(threading.Thread):

    def __init__(self, log_obj, sensor_alpha_module, motor_module):

        self.log = log_obj
        self. sensor_alpha = sensor_alpha_module
        self.motor_module = motor_module

        self.temp = None
        self.bat_volt = None
        self.current = None

    def get_sens_data(self):
        while True:
            self.temp = self.motor_module.get_tmp()
            self.current = self. sensor_alpha.get_csens()
            self.bat_volt = self.motor_module.get_ina219()
            #print("bat volt{}".format(self.bat_volt))
            time.sleep(5)

    def run(self):
        t1 = threading.Thread(target=self.get_sens_data)
        t1.start()
        t1.join()

    def get_tempereture(self):
        return self.temp

    def get_battery_voltage(self):
        volt = str.split(self.bat_volt)
        return self.bat_volt

    def get_current(self):
        return self.current


