#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import threading
import time
from datetime import datetime


class Shield(threading.Thread):
    """ The shield of Cube aka police  """

    def __init__(self, log_obj, scm, mcm, ccm):
        threading.Thread.__init__(self)
        self.log_obj = log_obj
        self.scm = scm
        self.mcm = mcm
        self.ccm = ccm
        self.mcm_watcher_active = None
        self.scm_watcher_active = None
        self.scm_reader_Active = None
        self.system_volt = None
        self.system_current = None
        self.main_current = None
        self.rm_temp = None
        self.lm_temp = None

        dt_string = datetime.now().strftime("%d_%m_%Y")
        self.statistics_logfile = "logs/cube_stats_logger_{}.log".format(dt_string)

    def __mcm_watcher(self):
        while self.mcm_watcher_active:
            if self.mcm.motor_active:
                t = time.time()
                if t - self.mcm.motor_active_time > 10:
                    self.log_obj.warning("Motor active for too long")
                    self.mcm.motor_drive("STOP")
            time.sleep(1)
        self.log_obj.debug("MCM watcher Stopping")
        self.mcm_watcher_active = False

    def __system_curent_check(self, status):

        if not self.system_current or not self.system_volt:
            if status:
                self.log_obj.warning("INA sensor is unavailable")
            return False

        if self.system_volt < 5:
            self.log_obj.warning("LOW bat volt{}".format(self.system_volt))
            self.scm.digital_out_mcp(3, "LOW")
            self.scm.digital_out_mcp(4, "HIGH")
            self.scm.digital_out_mcp(5, "LOW")
        else:
            self.scm.digital_out_mcp(3, "HIGH")
            self.scm.digital_out_mcp(4, "LOW")
            self.scm.digital_out_mcp(5, "LOW")
        if self.system_current > 2000:
            self.log_obj.critical("Dangerouse system current level detected, safety procedure initiated")
            self.safty_shut_down()
        return True

    def __motor_temp_check(self, status):

        if not self.lm_temp or self.rm_temp:
            if status:
                self.log_obj.warning("DHT  motor sensor is unavailable")
            return False

        if not self.lm_temp:
            self.log_obj.warning("DHT left motor sensor is unavailable")
            temp_left = list()
            temp_left.append("0")
            temp_left.append("0")
            temp_left.append("0")

        if self.rm_tem > 35:
            self.log_obj.critical("Dangerouse motor tempreture level detected {}, safety procedure initiated".format(self.rm_tem))
            self.safty_shut_down()

        return True

    def __main_curent_check(self, status):

        if not self.main_current:
            if status:
                self.log_obj.warning("Main Current is unavailable")
            return False

        if self.main_current > 15:
            self.log_obj.critical("Alarming main current level detected, {} A, initiating safety procedure".format(self.main_current))
        if self.main_current > 25:
            self.log_obj.critical("Dangerouse main current level detected, safety procedure initiated")
            self.safty_shut_down()

        return True

    def __scm_reader(self):

        while self.scm_reader_Active:
            # ina data ['volt', 'mA']
            ina_data = self.scm.get_ina219()
            if ina_data:
                self.system_volt = float(ina_data[0])
                self.system_current = float(ina_data[1])
            # dht11 ['hum', 'temp', 'hic' ]
            temp_left, temp_right = self.scm.get_tmp()
            if temp_left and temp_right:
                self.rm_temp = int(temp_right[1])
                self.lm_temp = int(temp_left[1])
            current = self.scm.get_csens()
            if current:
                self.main_current = current

            # volt, mA, A, left_motor_temp, right_motor_temp
            statistics_log = open(self.statistics_logfile, 'a')
            statistics_log.write("{} {} {} {} {}\n".format(self.system_volt, self.system_current, self.main_current, self.lm_temp, self.rm_temp))
            statistics_log.close()

    def __scm_watcher(self):

        syste_curent_ret = True
        main_current_ret = True
        motor_temp_ret = True

        while self.scm_watcher_active:
            syste_curent_ret = self.__system_curent_check(syste_curent_ret)
            main_current_ret = self.__main_curent_check(main_current_ret)
            motor_temp_ret = self.__motor_temp_check(motor_temp_ret)

        self.log_obj.debug("SCM watcher Stopping")
        self.scm_watcher_active = False

    def run(self):
        if self.mcm_watcher_active or self.scm_watcher_active:
            self.log_obj.warning("Shield watcher is allready running MCM {}, SCM {}".format(self.mcm_watcher_active, self.scm_watcher_active))
            return True
        self.mcm_watcher_active = True
        self.scm_watcher_active = True
        self.scm_reader_Active = True

        mcm_watcher = threading.Thread(target=self.__mcm_watcher)
        scm_watcher = threading.Thread(target=self.__scm_watcher)
        scm_reader = threading.Thread(target=self.__scm_reader)

        mcm_watcher.start()
        scm_reader.start()
        scm_watcher.start()

        self.log_obj.debug("Shield Watcher is operational")

    def safety_procedure(self):
        self.mcm.motor_drive("STOP")
        self.scm.sound_beep(1, 2)
        time.sleep(1)
        self.scm.sound_beep(1, 2)

    def safty_shut_down(self):
        self.mcm.motor_drive("STOP")
        self.mcm.enabled = False
        self.scm.sound_beep(1, 6)
