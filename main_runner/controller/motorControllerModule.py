#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Motor Controller Module
"""
import time


class MotorControllerModule:
    """ Handles the motor  """

    def __init__(self, log_obj, board, config, task_handler):
        self.state = 0
        self.logger = log_obj
        self.mutex = False
        self.enabled = False
        self.motor_active = False
        self.motor_active_time = None
        self.task_handler = task_handler

        if board:

            self.mc_enable = int(config["mcm"]["mcen"])
            self.lrpwm = int(config["mcm"]["lrpwm"])
            self.llpwm = int(config["mcm"]["llpwm"])
            self.rrpwm = int(config["mcm"]["rrpwm"])
            self.rlpwm = int(config["mcm"]["rlpwm"])
            self.lir = int(config["mcm"]["lir"])
            self.rir = int(config["mcm"]["rir"])

            self.board = board

            self.board.pinMode(self.mc_enable, "OUTPUT")
            self.board.pinMode(self.lrpwm, "OUTPUT")
            self.board.pinMode(self.llpwm, "OUTPUT")
            self.board.pinMode(self.rrpwm, "OUTPUT")
            self.board.pinMode(self.rlpwm, "OUTPUT")

            log_obj.debug("Initialized successfully".format())
            self.state = 1
        else:
            self.state = -1
            raise ValueError('Arduino not connected')

    def enable_motors(self, enable_mc=False):
        """ Enable both Motors
        """
        if not self.mutex:
            self.mutex = True
            if self.board and enable_mc:
                self.enabled = True
                self.logger.debug("Motors enabled".format())
                self.board.digitalWrite(self.mc_enable, "HIGH")
            else:
                self.enabled = False
                self.logger.debug("Motors disabled".format())
                self.board.digitalWrite(self.mc_enable, "LOW")
            self.mutex = False
        else:
            self.logger.debug("Mutex locked".format())

    def motor_drive(self, direction, speed=0):

        self.logger.debug("Drive {} {}".format(direction, speed))

        if self.enabled:
            if not self.mutex:
                self.mutex = True
                self.motor_active = True
                if direction == "RGT":
                    self.board.analogWrite(self.lrpwm, 70)
                    self.board.analogWrite(self.llpwm, 0)
                    self.board.analogWrite(self.rrpwm, 70)
                    self.board.analogWrite(self.rlpwm, 0)

                elif direction == "LFT":
                    self.board.analogWrite(self.lrpwm, 0)
                    self.board.analogWrite(self.llpwm, 70)
                    self.board.analogWrite(self.rrpwm, 0)
                    self.board.analogWrite(self.rlpwm, 70)

                elif direction == "FWD":
                    self.board.analogWrite(self.lrpwm, speed)
                    self.board.analogWrite(self.llpwm, 0)
                    self.board.analogWrite(self.rrpwm, 0)
                    self.board.analogWrite(self.rlpwm, speed)

                elif direction == "RWS":
                    self.board.analogWrite(self.lrpwm, 0)
                    self.board.analogWrite(self.llpwm, 70)
                    self.board.analogWrite(self.rrpwm, 70)
                    self.board.analogWrite(self.rlpwm, 0)

                elif direction == "STOP":
                    self.motor_active = False
                    self.board.analogWrite(self.lrpwm, 0)
                    self.board.analogWrite(self.llpwm, 0)
                    self.board.analogWrite(self.rrpwm, 0)
                    self.board.analogWrite(self.rlpwm, 0)
                else:
                    self.logger.warning("Unkown drive command {}".format(direction))
                self.mutex = False
                self.motor_active_time = time.time()
            else:
                self.logger.debug("Mutex locked".format())
        else:
            self.logger.warning("Operation_status".format(self.enabled))
