#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time


class SensorControllerModule:

    def __init__(self,  log_obj, board, config, task_handler):

        #  "ACS758LCB-050B", for model use 0
        #  "ACS758LCB-050U", for model use 1
        #  "ACS758LCB-100B", for model use 2
        #  "ACS758LCB-100U", for model use 3
        # "ACS758KCB-150B", for model use 4
        #  "ACS758KCB-150U", for model use 5
        #  "ACS758ECB-200B", for model use 6
        #  "ACS758ECB-200U" for model use  7

        self.state = 0

        if board:
            self.logger = log_obj
            self.board = board
            self.mutex = False
            self.task_handler = task_handler

            self.csens_pin = int(config["scm"]["hcs"])
            self.sound_pin = int(config["scm"]["buzzer"])
            self.right_dht = int(config["scm"]["temp1"])
            self.left_dht = int(config["scm"]["temp2"])

            self.board.pinMode(self.csens_pin, "INPUT")

            self.csens_vcc_level = 5.0
            self.csens_model = 2
            self.csens_cut_off_limit = 0.10
            self.csens_sensitivity = [40.0, 60.0, 20.0, 40.0, 13.3, 16.7, 10.0, 20.0]
            self.csens_quiescent_output_voltage = [0.5, 0.12, 0.5, 0.12, 0.5, 0.12, 0.5, 0.12]
            self.csens_factor = self.csens_sensitivity[self.csens_model]/1000
            self.csens_qov = self.csens_quiescent_output_voltage[self.csens_model]*self.csens_vcc_level
            self.csens_voltage = 0
            self.csens_cutoff = self.csens_factor/self.csens_cut_off_limit

            self.board.pinModeMCP(0, "OUTPUT")
            self.board.pinModeMCP(1, "OUTPUT")
            self.board.pinModeMCP(2, "OUTPUT")
            self.board.pinModeMCP(3, "OUTPUT")
            self.board.pinModeMCP(7, "OUTPUT")
            self.board.pinModeMCP(5, "OUTPUT")
            self.board.pinModeMCP(6, "OUTPUT")

            self.digital_out_mcp(0, "HIGH")
            log_obj.debug("Initialized successfully".format())
            self.state = 1
        else:
            self.state = -1
            raise ValueError('Arduino not connected')

    def get_state(self):
        return self.state

    def get_ina219(self):

        if not self.mutex:
            self.mutex = True
            ina219 = self.board.ina219()
            self.mutex = False
            if len(ina219) > 1:
                return str.split(ina219[0])
            else:
                return None
        else:
            self.logger.warning("Mutex locked".format())

    def get_csens(self):

        voltage_raw = (5.0 / 1023.0) * self.board.analogRead(self.csens_pin)
        voltage = voltage_raw - self.csens_qov + 0.012
        current = voltage / self.csens_factor
        if current < self.csens_cutoff:
            return None
        return current

    def sound_beep(self, delay, loop):

        for l in range(loop):
            self.board.analogWrite(self.sound_pin, 128)
            time.sleep(delay)
            self.board.analogWrite(self.sound_pin, 0)
            time.sleep(delay)
            self.board.analogWrite(self.sound_pin, 128)
        self.board.analogWrite(self.sound_pin, 0)

    def digital_out(self, pin, status):
        self.board.digitalWrite(pin, status)

    def digital_out_mcp(self, pin, status):
        self.board.digitalMCPWrite(pin, status)

    def reset_mcp_out(self):
        for pin in range(16):
            self.digital_out_mcp(pin, "LOW")

    def get_tmp(self):
        """ Motor controller module get temperature
        """
        if not self.mutex:
            self.mutex = True
            left_tmp = self.board.dht(self.left_dht, 0)
            right_tmp = self.board.dht(self.right_dht, 0)
            self.mutex = False
            return left_tmp, right_tmp
        else:
            self.logger.warning("Mutex locked".format())
            return None, None

    def shutdown(self):
        for pin in range(16):
            self.board.digitalMCPWrite(pin, "LOW")
