#!/usr/bin/env python3
import threading


class ComControllerModule(threading.Thread):
    """ Handles the RF  """

    def __init__(self, log_obj, board, config, task_handler):
        threading.Thread.__init__(self)
        self.state = 0
        self.log_obj = log_obj
        self.config = config
        self.task_handler = task_handler
        self.mutex = False
        self.ccm_active = None

        if board:
            self.board = board
            log_obj.debug("Initialized successfully".format())
            self.state = 1
        else:
            self.state = -1
            raise ValueError('Arduino not connected')

    def run(self):
        self.log_obj.debug("Starting Communication Module")
        self.ccm_active = True
        led_toggle = True

        while self.ccm_active:
            rx_data = self.board.read_serial()
            data = rx_data.split(" ")

            if len(data) == 1:
                if "ACK" in data[0]:
                    if led_toggle:
                        led_toggle = False
                        self.task_handler.add_tasks("MSG CCM SCM {} HIGH 0 0".format(data[0]))
                    else:
                        led_toggle = True
                        self.task_handler.add_tasks("MSG CCM SCM {} LOW 0 0".format(data[0]))

            elif len(data) == 2:
                if "up" or "down" or "left" or "right" or "stop" in data[0]:
                    self.task_handler.add_tasks("MSG CCM MCM {} {} 0 0".format(data[0], data[1]))
                else:
                    self.log_obj.warning("Invalid RX data {}", data[0])
            else:
                self.log_obj.warning("Invalid RX data")

        self.ccm_active = False
        self.log_obj.debug("Stopping Communication Module")

    def get_state(self):
        return self.state
