#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import threading


class Terminal(threading.Thread):

    def __init__(self, log_obj):
        threading.Thread.__init__(self)
        self.log_obj = log_obj

    def run(self):
        while True:
            for line in sys.stdin:
                print(">>>>>>>>>>>>>>>>>>> {}".format(line))


if __name__ == "__main__":
    terminal = Terminal(None)
    terminal.run()
