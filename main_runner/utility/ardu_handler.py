#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import os
import time

from utility.arduino import Arduino


class ArduHandler:

    def __init__(self, log_obj, config):

        self.log_obj = log_obj
        self.boards = []
        self.com1 = None
        self.com2 = None

        if sys.platform == "linux":
            if os.uname()[4][:3] == "x86":
                print("Running on Linux x86 platform")
                self.com1 = config["arduinos"]["linux_com1"]
                self.com2 = config["arduinos"]["linux_com2"]
                self.com3 = config["arduinos"]["linux_com3"]

            elif os.uname()[4][:3] == "arm":
                print("Running on Linux rpi platform")
                import RPi.GPIO as GPIO
                os.system("echo '1-1' | sudo tee /sys/bus/usb/drivers/usb/bind")
                #  Wait for Arduino boots up
                log_obj.info("Wait for Arduino to boot up...")
                self.com1 = config["arduinos"]["linux_com1"]
                self.com2 = config["arduinos"]["linux_com2"]
                self.com3 = config["arduinos"]["linux_com3"]
                time.sleep(5)
            else:
                raise ValueError("Invalid comport")
        else:
            print("Running on a windows platform")
            self.com1 = config["arduinos"]["win_com1"]
            self.com2 = config["arduinos"]["win_com2"]
            self.com3 = config["arduinos"]["win_com3"]

        self.arduino_comport_list = [self.com1, self.com2, self.com3]

    def connect(self):

        for port in self.arduino_comport_list:
            self.log_obj.debug("Connecting Arduino with port [{}]".format(port))
            try:
                self.boards.append(Arduino('115200', port=port))
            except Exception as e:
                self.log_obj.error("Connecting Arduino to port {} failed ".format(port))
                self.log_obj.error("{}".format(e))
                raise ValueError(e)
