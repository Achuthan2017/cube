#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import threading
import queue


class TaskHandler(threading.Thread):
    """ Task handler  """

    def __init__(self, log_obj):
        threading.Thread.__init__(self)
        self.log_obj = log_obj
        self.mcm = None
        self.scm = None
        self.ccm = None
        self.tasks = queue.Queue(maxsize=1024)
        self.active = None

        self.log_obj.debug("Initialized successfully".format())

    def update_modules(self, mcm, scm, ccm):
        self.mcm = mcm
        self.scm = scm
        self.ccm = ccm

    def add_tasks(self, task):
        if self.tasks.full():
            self.log_obj.warning("Task Queue is full")
            return False
        #self.log_obj.debug("Add task {}".format(task))
        self.tasks.put(task)

    def run(self):
        self.active = True
        while self.active:
            if not self.tasks.empty():
                task = self.tasks.get(block=False)
                self.process_task(task)
        self.active = False
        self.log_obj.debug("Stopping Task Handler")

    def mcm_task(self, data, arg1, arg2, arg3):
        """ MCM tasks handler """

        self.log_obj.debug("TASK {} {}".format(data, arg1))

        if "up" in data:
            self.mcm.motor_drive("FWD", int(arg1))
        elif "down" in data:
            self.mcm.motor_drive("RWS", int(arg1))
        elif "left" in data:
            self.mcm.motor_drive("LFT", int(arg1))
        elif "right" in data:
            self.mcm.motor_drive("RGT", int(arg1))
        elif "stop" in data:
            self.mcm.motor_drive("STOP")
        else:
            self.log_obj.warning("Invalid data {}".format(data))

    def scm_task(self, data, arg1, arg2, arg3):
        """ SCM tasks handler """
        if data == "ACK":
            self.scm.digital_out_mcp(2, arg1)

    def ccm_task(self, data, arg1, arg2, arg3):
        """ CCM tasks handler """

    def process_task(self, task):
        """
            process task that are posted by other handlers.
            task protocol: <Header> <FROM> <TO> <DATA> <ARG1> <ARG2> <ARG3>
        """
        msg = str(task).split(" ")
        if len(msg) < 7:
            self.log_obj.warning("Invalid task recieved < {} >".format(task))
            return False
        #self.log_obj.debug("Process Task: {} | {} {} {} {} {} {} {}".format(task, msg[0],msg[1], msg[2],msg[3], msg[4], msg[5], msg[6]))

        if msg[2] == "MCM":
            self.mcm_task(msg[3], msg[4], msg[5], msg[6])
        elif msg[2] == "SCM":
            self.scm_task(msg[3], msg[4], msg[5], msg[6])
        elif msg[2] == "CCM":
            self.ccm_task(msg[3], msg[4], msg[5], msg[6])
