import sys
import os
import subprocess
import RPi.GPIO as GPIO
import configparser
import atexit
import time
import threading

from logs import logger

from utility.ardu_handler import ArduHandler
from controller.sensorControllerModule import SensorControllerModule as Scm


GPIO_START_PIN = 4
GPIO_START_LED = 26

GPIO_STOP_PIN = 17
GPIO_STOP_LED = 19


def blink(pin):
    while blink_event:
        GPIO.output(pin, GPIO.HIGH)
        time.sleep(0.5)
        GPIO.output(pin, GPIO.LOW)
        time.sleep(0.5)


def shutdown_ardus(c, log):

    print("Shutting Down arduino !!!!!")

    os.system("fuser -k /dev/ttyUSB0")
    time.sleep(1)
    os.system("fuser -k /dev/ttyUSB1")
    time.sleep(1)
    os.system("fuser -k /dev/ttyUSB2")
    time.sleep(1)
    os.system("echo 'USB Unbind' | sudo tee /sys/bus/usb/drivers/usb/unbind")
    time.sleep(2)
    os.system("echo 'USB bind' | sudo tee /sys/bus/usb/drivers/usb/bind")

    arduino = ArduHandler(log, c)
    arduino.connect()

    for arduino in arduino.boards:
        arduino.sr.flushInput()
        mode = arduino.mode()

        if mode == "scm_V0_1_0":
            scm = Scm(log, arduino, c)
            scm.shutdown()


def cleanexit():
    print("Clean Exiting....")
    if sys.platform == "linux":
        os.system("echo 'USB Unbind' | sudo tee /sys/bus/usb/drivers/usb/unbind")
        time.sleep(2)
        os.system("echo 'USB bind' | sudo tee /sys/bus/usb/drivers/usb/bind")


if __name__ == '__main__':

    os.chdir("/share/work/cube/main_runner/")

    atexit.register(cleanexit)

    GPIO.setmode(GPIO.BCM)

    GPIO.setup(GPIO_START_LED, GPIO.OUT)
    GPIO.output(GPIO_START_LED, GPIO.LOW)

    GPIO.setup(GPIO_STOP_LED, GPIO.OUT)
    GPIO.output(GPIO_STOP_LED, GPIO.LOW)

    GPIO.setup(GPIO_START_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(GPIO_STOP_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    started = False
    stopped = True
    pro = None

    blink_event = False

    config = configparser.ConfigParser()
    config.read('ardu_config.txt')
    log_obj = logger.initiate_logging()

    while True:

        if not GPIO.input(GPIO_START_PIN) and not started:

            GPIO.output(GPIO_STOP_LED, GPIO.LOW)
            blink_event = True
            t = threading.Thread(target=blink, args=(GPIO_START_LED,))
            t.start()
            stopped = False
            started = True
            pro = subprocess.Popen(['python3.8', './cube.py'])
            time.sleep(10)
            blink_event = False
            time.sleep(2)
            GPIO.output(GPIO_START_LED, GPIO.HIGH)

        if not GPIO.input(GPIO_STOP_PIN) and not stopped:
            stopped = True
            blink_event = True
            t = threading.Thread(target=blink, args=(GPIO_STOP_LED,))
            t.start()

            GPIO.output(GPIO_START_LED, GPIO.LOW)
            started = False
            pro.kill()
            #shutdown_ardus(config, log_obj)
            time.sleep(5)
            blink_event = False

            GPIO.output(GPIO_STOP_LED, GPIO.HIGH)

GPIO.cleanup()
