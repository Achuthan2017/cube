#!/usr/bin/env python
import time
import sys
import atexit
import os
import configparser
import threading

if sys.platform == "linux":
    import RPi.GPIO as GPIO

from logs import logger
from utility.ardu_handler import ArduHandler
from utility.task_handler import TaskHandler
from controller.motorControllerModule import MotorControllerModule as Mcm
from controller.sensorControllerModule import SensorControllerModule as Scm
from controller.communicationControllerModule import ComControllerModule as Ccm
from shield import Shield


blink_event = False


class Cube(threading.Thread):

    def __init__(self, log_obj, config):
        threading.Thread.__init__(self)
        self.log_obj = log_obj
        self.config = config
        self.scm = None
        self.mcm = None
        self.ccm = None
        self.shield = None
        self.task_handler = None
        self.c_start_initiate = False
        self.c_started = False
        self.c_stop = False
        self.c_stopped = True
        self.gpio_start_pin = 4
        self.gpio_start_led = 26
        self.gpio_stop_pin = 17
        self.gpio_stop_led = 19

        if sys.platform == "linux":
            self.setup_gpio()

    def setup_gpio(self):

        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.gpio_start_led, GPIO.OUT)
        GPIO.setup(self.gpio_stop_led, GPIO.OUT)

        GPIO.setup(self.gpio_start_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(self.gpio_stop_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    def reactivate_cube(self):
        self.shield.mcm_watcher_active = True
        self.shield.scm_watcher_active = True
        self.shield.scm_reader_Active = True
        self.ccm.run()
        self.shield.run()
        self.run_cube()

    def run_cube(self):

        self.log_obj.info("CUBE is operational")

        self.mcm.enable_motors(True)
        self.scm.sound_beep(1, 2)
        self.scm.digital_out_mcp(1, "HIGH")

        while not self.c_stop:
            time.sleep(2)

        self.shutdown_proceduer()

    def shutdown_proceduer(self):
        self.log_obj.debug("Shutdown procedure initiated")
        self.ccm.ccm_active = False
        time.sleep(1)
        self.scm.reset_mcp_out()
        self.shield.mcm_watcher_active = False
        self.shield.scm_watcher_active = False
        self.shield.scm_reader_Active = False
        self.ccm.ccm_active = False
        self.task_handler.active = False
        self.mcm.board.sr.close()
        self.scm.board.sr.close()
        self.ccm.board.sr.close()
        self.log_obj.debug("Cube Stopped")
        self.c_stopped = True

    def run(self):

        arduino = ArduHandler(self.log_obj, self.config)
        arduino.connect()
        self.c_stopped = False

        self.task_handler = TaskHandler(self.log_obj)

        for arduino in arduino.boards:
            time.sleep(1)
            arduino.sr.flushInput()
            mode = arduino.mode()
            self.log_obj.debug("Arduino mode [{}]".format(mode))

            if mode == "scm_V0_1_0":
                self.log_obj.debug("Initializing < Sensor Controller Module > to arduino port < {} >".format(arduino.sr.name))
                self.scm = Scm(self.log_obj, arduino, self.config, self.task_handler)

            elif mode == "mcm_V0_1_0":
                self.log_obj.debug("Initializing < Motor Controller Module > to arduino port < {} >".format(arduino.sr.name))
                self.mcm = Mcm(self.log_obj, arduino, self.config, self.task_handler)

            elif mode == "ccm_V0_1_0":
                self.log_obj.debug("Initializing < Communication Controller Module > to arduino port < {} >".format(arduino.sr.name))
                self.ccm = Ccm(self.log_obj, arduino, self.config, self.task_handler)
            else:
                self.log_obj.error("Invalid mode {}".format(mode))
                raise ValueError('Invalid mode')

        if self.scm and self.mcm and self.ccm:
            self.log_obj.debug("Initiating Shield Watcher")
            self.shield = Shield(self.log_obj, self.scm, self.mcm, self.ccm)
            self.shield.daemon = True
            self.shield.start()
            if self.scm.state == 1 and self.mcm.state == 1 and self.ccm.state == 1:
                self.ccm.start()
                self.task_handler.update_modules(self.mcm, self.scm, self.ccm)
                self.task_handler.start()
                self.c_started = True
                self.run_cube()
            else:
                self.log_obj.error("Arduino invalid state SCM {} | MCM {}".format(self.scm.state, self.mcm.state))
        else:
            self.log_obj.error("Arduino Module Initialization failed")
            raise ValueError("Arduino Module Initialization failed")


def blink(pin):
    while blink_event:
        if sys.platform == "linux":
            GPIO.output(pin, GPIO.HIGH)
            time.sleep(0.1)
            GPIO.output(pin, GPIO.LOW)
            time.sleep(0.1)
        else:
            print("Blink")
            time.sleep(1)


def cleanexit():
    print("Clean Exiting....")
    if sys.platform == "linux":
        os.system("echo 'USB Unbind' | sudo tee /sys/bus/usb/drivers/usb/unbind")
        time.sleep(2)
        os.system("echo 'USB bind' | sudo tee /sys/bus/usb/drivers/usb/bind")


def main_win(log_obj, config):

    loop_cnt = 0
    stop_cnt = 0

    cube = Cube(log_obj, config)
    cube.c_start_initiate = True

    print("IN WINDOWS")
    global blink_event

    while True:
        if cube.c_start_initiate and cube.c_stopped:
            loop_cnt = 0
            stop_cnt = 0
            cube.c_start_initiate = False
            blink_event = True
            blink_t = threading.Thread(target=blink, args=(cube.gpio_stop_led,))
            blink_t.daemon = True
            blink_t.start()
            if not cube.is_alive():
                cube = Cube(log_obj, config)
                cube.daemon = True
                cube.start()
            while not cube.c_started:
                time.sleep(1)
            blink_event = False

        if cube.c_stop and not cube.c_start_initiate:
            blink_event = True
            blink_t = threading.Thread(target=blink, args=(cube.gpio_stop_led,))
            blink_t.daemon = True
            blink_t.start()
            while not cube.c_stopped:
                time.sleep(1)
            cube.c_stop = False
            blink_event = False

        time.sleep(1)
        #print("Loop loop_cnt {}, stop_cnt {}".format(loop_cnt, stop_cnt))

        #loop_cnt += 1
        if loop_cnt == 15:
            cube.c_start_initiate = True

        #stop_cnt += 1
        if stop_cnt == 5:
            cube.c_stop = True


def main():

    config = configparser.ConfigParser()
    config.read('ardu_config.txt')
    log_obj = logger.initiate_logging()

    if sys.platform == "win32":
        main_win(log_obj, config)
    else:
        cube = Cube(log_obj, config)
        cube.c_start_initiate = True
        global blink_event

        while True:
            if not GPIO.input(cube.gpio_start_pin) and cube.c_stopped:
                log_obj.debug("Start Pressed")
                GPIO.output(cube.gpio_stop_led, GPIO.LOW)
                blink_event = True
                blink_t = threading.Thread(target=blink, args=(cube.gpio_start_led,))
                blink_t.daemon = True
                blink_t.start()
                cube.c_start_initiate = False
                if not cube.is_alive():
                    print("Cube Thread Not startet")
                    cube = Cube(log_obj, config)
                    cube.daemon = True
                    cube.start()
                while not cube.c_started:
                    print("Not startet")
                    time.sleep(1)
                print("Cube startet")
                blink_event = False
                time.sleep(2)
                GPIO.output(cube.gpio_start_led, GPIO.HIGH)

            if not GPIO.input(cube.gpio_stop_pin) and not cube.c_stopped:
                log_obj.debug("Stop Pressed")
                cube.c_stop = True
                GPIO.output(cube.gpio_start_led, GPIO.LOW)
                cube.blink_event = True
                blink_t = threading.Thread(target=blink, args=(cube.gpio_stop_led,))
                blink_t.daemon = True
                blink_t.start()
                while not cube.c_stopped:
                    print("Not Stopped")
                    time.sleep(1)
                print("Cube Stopped")
                blink_event = False
                cube.c_stop = False
                time.sleep(2)
                GPIO.output(cube.gpio_stop_led, GPIO.HIGH)
            time.sleep(1)
            print("Loop")


if __name__ == "__main__":
    atexit.register(cleanexit)
    main()
