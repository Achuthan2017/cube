#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
from datetime import datetime
import sys


def initiate_logging():
    # log_obj.info("yes   hfghghg ghgfh".format())
    # log_obj.critical("CRIC".format())
    # log_obj.error("ERR".format())
    # log_obj.warning("WARN".format())
    # log_obj.debug("debug".format())

    logging_level = logging.DEBUG

    dt_string = datetime.now().strftime("%d_%m_%Y")
    file_name = "logs/cube_logger_{}".format(dt_string)
    formatter = logging.Formatter(fmt='%(asctime)s %(module)s,line: %(lineno)d %(levelname)8s | %(message)s',
                                  datefmt='%Y/%m/%d %H:%M:%S')  # %I:%M:%S %p AM|PM format
    logging.basicConfig(filename='%s.log' % file_name, format='%(asctime)s %(module)s,line: %(lineno)d %(levelname)8s | %(message)s',
                        datefmt='%Y/%m/%d %H:%M:%S', filemode='a+', level=logging.INFO)
    log_obj = logging.getLogger()
    log_obj.setLevel(logging_level)
    # log_obj = logging.getLogger().addHandler(logging.StreamHandler())

    # console printer
    screen_handler = logging.StreamHandler(stream=sys.stdout)  # stream=sys.stdout is similar to normal print
    screen_handler.setFormatter(formatter)
    logging.getLogger().addHandler(screen_handler)

    log_obj.info("Logger object created successfully..")
    return log_obj
