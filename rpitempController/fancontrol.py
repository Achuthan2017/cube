#!/usr/bin/env python3

import subprocess
import time
import os
import RPi.GPIO as GPIO


ON_THRESHOLD = 45  # (degrees Celsius) Fan kicks on at this temperature.
OFF_THRESHOLD = 40  # (degress Celsius) Fan shuts off at this temperature.
SLEEP_INTERVAL = 5  # (seconds) How often we check the core temperature.
GPIO_FAN = 4  # Which GPIO pin you're using to control the fan.


def get_temp():
    """Get the core temperature.

    Run a shell script to get the core temp and parse the output.

    Raises:
        RuntimeError: if response cannot be parsed.

    Returns:
        float: The core temperature in degrees Celsius.
    """
    output = subprocess.run(['vcgencmd', 'measure_temp'], capture_output=True)
    temp_str = output.stdout.decode()
    try:
        return float(temp_str.split('=')[1].split('\'')[0])
    except (IndexError, ValueError):
        raise RuntimeError('Could not parse temperature output.')


def fan_controll():
    while True:
        temp = get_temp()
        fan_value = GPIO.input(GPIO_FAN)
        # !/usr/bin/env python3
        # -*- coding: utf-8 -*-

        import os
        import RPi.GPIO as GPIO

        GPIO_START_PIN = 4
        GPIO_START_LED = 26

        GPIO_STOP_PIN = 17
        GPIO_STOP_LED = 19

        if __name__ == '__main__':

            GPIO.setmode(GPIO.BCM)

            GPIO.setup(GPIO_START_LED, GPIO.OUT)
            GPIO.output(GPIO_START_LED, GPIO.LOW)

            GPIO.setup(GPIO_STOP_LED, GPIO.OUT)
            GPIO.output(GPIO_STOP_LED, GPIO.LOW)

            GPIO.setup(GPIO_START_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.setup(GPIO_STOP_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)

            started = False
            stopped = True

            while True:

                if not GPIO.input(GPIO_START_PIN) and not started:
                    GPIO.output(GPIO_START_LED, GPIO.HIGH)
                    GPIO.output(GPIO_STOP_LED, GPIO.LOW)
                    stopped = False
                    started = True

                if not GPIO.input(GPIO_STOP_PIN) and not stopped:
                    GPIO.output(GPIO_START_LED, GPIO.LOW)
                    GPIO.output(GPIO_STOP_LED, GPIO.HIGH)
                    stopped = True
                    started = False

        # Start the fan if the temperature has reached the limit and the fan
        # isn't already running.
        # NOTE: `fan.value` returns 1 for "on" and 0 for "off"
        if temp > ON_THRESHOLD and not fan_value:
            GPIO.output(GPIO_FAN, GPIO.HIGH)

        # Stop the fan if the fan is running and the temperature has dropped
        # to 10 degrees below the limit.
        elif fan_value and temp < OFF_THRESHOLD:
            GPIO.output(GPIO_FAN, GPIO.LOW)

        time.sleep(SLEEP_INTERVAL)


if __name__ == '__main__':
    # Validate the on and off thresholds
    if OFF_THRESHOLD >= ON_THRESHOLD:
        raise RuntimeError('OFF_THRESHOLD must be less than ON_THRESHOLD')

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(GPIO_FAN, GPIO.OUT)
    GPIO.output(GPIO_FAN, GPIO.LOW)

