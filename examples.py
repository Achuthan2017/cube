#!/usr/bin/env python
from Arduino import Arduino
import time


def Blink(led_pin, baud, port=""):
    """
    Blinks an LED in 1 sec intervals
    """
    board = Arduino(baud, port=port)
    board.pinMode(led_pin, "OUTPUT")
    while True:
        board.digitalWrite(led_pin, "LOW")
        print(board.digitalRead(led_pin))  # confirm LOW (0)
        time.sleep(1)
        board.digitalWrite(led_pin, "HIGH")
        print(board.digitalRead(led_pin))  # confirm HIGH (1)
        time.sleep(5)


def getdht(baud, port=""):
    board = Arduino(baud, port=port)
    while True:
        # DHT sensor example
        pin = 2
        sensorType = 0
        data = board.dht(pin, sensorType)
        if len(data) == 3:
            print(data)
            [humidity, temperature, heatIndex] = data

            reply = "Humidity = " + str(humidity) + " % \t"
            reply += "Temperature = " + str(temperature) + " ˙C \t"
            reply += "Heat Index = " + str(heatIndex) + " ˙C"

            print(reply)
            time.sleep(2)


def softBlink(led_pin, baud, port=""):
    """
    Fades an LED off and on, using
    Arduino's analogWrite (PWM) function
    """
    board = Arduino(baud, port=port)
    i = 0
    while True:
        i += 1
        k = i % 510
        if k % 5 == 0:
            if k > 255:
                k = 510 - k
            board.analogWrite(led_pin, k)


def get_voltage(board, port=""):

    left_tmp = board.ina219()
    print(left_tmp)


def adjustBrightness(pot_pin, led_pin, baud, port=""):
    """
    Adjusts brightness of an LED using a
    potentiometer.
    """
    board = Arduino(baud, port=port)
    while True:
        time.sleep(0.01)
        val = board.analogRead(pot_pin) / 4
        print(val)
        board.analogWrite(led_pin, val)


def PingSonar(pw_pin, baud, port=""):
    """
    Gets distance measurement from Ping)))
    ultrasonic rangefinder connected to pw_pin
    """
    board = Arduino(baud, port=port)
    pingPin = pw_pin
    while True:
        duration = board.pulseIn(pingPin, "HIGH")
        inches = duration / 72. / 2.
        # cent = duration / 29. / 2.
        print(inches, "inches")
        time.sleep(0.1)


def LCD(tx, baud, ssbaud, message, port=""):
    """
    Prints to two-line LCD connected to
    pin tx
    """
    board = Arduino(baud, port=port)
    board.SoftwareSerial.begin(0, tx, ssbaud)
    while True:
        board.SoftwareSerial.write(" test ")


def drive_forward():
    board = Arduino('115200', port="com8")
    board.digitalWrite(4, "HIGH")
    board.analogWrite(3, 255)
    board.analogWrite(5, 255)
    board.analogWrite(6, 255)
    board.analogWrite(9, 255)
    while True:
        pass


def mcp_test(baud, port=""):

    board = Arduino(baud, port=port)

    board.pinModeMCP(0, "OUTPUT")
    board.pinModeMCP(1, "OUTPUT")
    board.pinModeMCP(2, "OUTPUT")
    board.pinModeMCP(3, "OUTPUT")
    board.pinModeMCP(4, "OUTPUT")
    board.pinModeMCP(5, "OUTPUT")
    board.pinModeMCP(6, "OUTPUT")
    board.pinModeMCP(7, "OUTPUT")

    board.pinModeMCP(8, "OUTPUT")
    board.pinModeMCP(9, "OUTPUT")
    board.pinModeMCP(10, "OUTPUT")
    board.pinModeMCP(11, "OUTPUT")
    board.pinModeMCP(12, "OUTPUT")
    board.pinModeMCP(13, "OUTPUT")
    board.pinModeMCP(14, "OUTPUT")
    board.pinModeMCP(15, "OUTPUT")



    while True:
        for i in range(16):
            board.digitalMCPWrite(i, "HIGH")
            get_voltage(board, "COM19")
            time.sleep(1)

        for i in range(16):
            board.digitalMCPWrite(i, "LOW")
            get_voltage(board, "COM19")
            time.sleep(1)


def read_serial_com(baud, port=""):
    board = Arduino(baud, port=port)

    while True:
        rx_data = board.read_serial()
        print(rx_data)



if __name__ == "__main__":
    #get_voltage('115200', "COM19")
    #softBlink(9, '115200', "COM8")
    #Blink(6, '115200')
    #drive_forward()
    #mcp_test('115200', "COM19")
    read_serial_com('115200', "COM16")
